//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: yaTom
// A command-line utility, with a semi-graphic interface, to generate a self-extractable archive.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <libgen.h>
#include <unistd.h>
#include <pwd.h>
#include <sys/types.h>
//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "zoe.h"
#include "tom.h"
#include "yaTom_cmdline.h"
//------------------------------------------------------------------------------
// MACROS
//------------------------------------------------------------------------------
#define TITLE 						"SELF-EXTRACTABLE ARCHIVE GENERATION"
#define FOREGROUND					white

#define	MAX_ARCHIVENAME				50
#define MAX_APPLICATIONNAME			50
#define MAX_APPLICATIONREVISION		50
#define MAX_APPLICATIONDATE			50
#define MAX_APPLICATIONDESCRIPTION	100

#define	Q1							"Application Archive Name................................. "
#define Q1_HELP						"Name of the file that will be generated. It is suggested to give a name related to (or even recalling) the name of the application."

#define	Q2							"Application Name......................................... "
#define Q2_HELP						"Name of the application you wish to package."

#define	Q3							"Application Revision..................................... "
#define Q3_HELP						"Revision (or version) of the application you wish to package."

#define	Q4							"Application Date......................................... "
#define Q4_HELP						"Date of the application you wish to package."

#define	Q5							"Application Description.................................. "
#define Q5_HELP						"One-line description of the application you wish to package."

#define Q6 							"Application Pathname..................................... "

#define Q7							"Application License File................................. "

#define Q8							"Is This License Linked With Free Software Development.... "

#define Q9							"Is There A Script To Run In The Installation Directory... "

#define Q10							"Post-install Script File ................................ "

#define Q11 						"Are we launching the archive generation.................. "

#define ZOE_REPORT(n, abort, back)	if (n == ZOE_ABORT) goto abort; \
									if (n == ZOE_BACK) goto back;
//------------------------------------------------------------------------------
// MAIN
//------------------------------------------------------------------------------
int main(int argc, char **argv) {
	struct gengetopt_args_info	args_info;
	struct s_archive archiveParam;
	int errorCode, report, izoe, iline;
	objectID id;
	char *homedir;
	char archiveName[MAX_ARCHIVENAME];
	char applicationName[MAX_APPLICATIONNAME];
	char applicationRevision[MAX_APPLICATIONREVISION];
	char applicationDate[MAX_APPLICATIONDATE];
	char applicationDescription[MAX_APPLICATIONDESCRIPTION];
	char applicationPath[PATH_MAX];
	char applicationLicenseFile[PATH_MAX];
	char applicationPostInstallationScript[PATH_MAX];
	int lines[10];

	const char *yesno[] = { "Yes", "No" };
	//---- Parameter checking and setting --------------------------------------
	if (cmdline_parser_yaTom(argc, argv, &args_info) != 0)
		return EXIT_FAILURE;
	//----  Go on --------------------------------------------------------------
	if ((homedir = getenv("HOME")) == NULL)
		homedir = getpwuid(getuid())->pw_dir;
	report = 0;
	zoe_init(20, 120, WITHOUT_LOG);

	zoe_setattributes(ZOE_OBJECT_TITLE | ZOE_OBJECT_ANSWER | ZOE_OBJECT_GONOGO, FOREGROUND, boldon);
	zoe_setattributes(ZOE_OBJECT_ERROR, red, boldon);
	zoe_setattributes(ZOE_OBJECT_QUESTION | ZOE_OBJECT_TEXT, FOREGROUND, boldoff);

	zoe_title(TITLE);
	lines[0] = zoe_getnextfreeline();
LQ1:
	izoe = zoe_open_question(&id, lines[0], 1, Q1, Q1_HELP, archiveName, sizeof(archiveName));
	ZOE_REPORT(izoe, ABORT, ABORT)
	if (strlen(archiveName) == 0) {
		zoe_error("Application archive name cannot be empty!");
		goto LQ1;
	}
	archiveParam.archiveName = archiveName;
	lines[1] = zoe_getnextfreeline();
LQ2:
	izoe = zoe_open_question(&id, lines[1], 1, Q2, Q2_HELP, applicationName, sizeof(applicationName));
	ZOE_REPORT(izoe, ABORT, LQ1)
	if (strlen(applicationName) == 0) {
		zoe_error("Application name cannot be empty!");
		goto LQ2;
	}
	archiveParam.applicationName = applicationName;
	lines[2] = zoe_getnextfreeline();
LQ3:
	izoe = zoe_open_question(&id, lines[2], 1, Q3, Q3_HELP, applicationRevision, sizeof(applicationRevision));
	ZOE_REPORT(izoe, ABORT, LQ2)
	if (strlen(applicationRevision) == 0) {
		zoe_error("Application revision cannot be empty!");
		goto LQ3;
	}
	archiveParam.applicationRevision = applicationRevision;
	lines[3] = zoe_getnextfreeline();
LQ4:
	izoe = zoe_open_question(&id, lines[3], 1, Q4, Q4_HELP, applicationDate, sizeof(applicationDate));
	ZOE_REPORT(izoe, ABORT, LQ3)
	if (strlen(applicationDate) == 0) {
		zoe_error("Application date cannot be empty!");
		goto LQ4;
	}
	archiveParam.applicationDate = applicationDate;
	lines[4] = zoe_getnextfreeline();
LQ5:
	izoe = zoe_open_question(&id, lines[4], 1, Q5, Q5_HELP, applicationDescription, sizeof(applicationDescription));
	ZOE_REPORT(izoe, ABORT, LQ4)
	if (strlen(applicationDescription) == 0) {
		zoe_error("Application description cannot be empty!");
		goto LQ5;
	}
	archiveParam.applicationDescription = applicationDescription;
	lines[5] = zoe_getnextfreeline();
L06:
	izoe = zoe_file_chooser(&id, lines[5], 1, Q6, homedir, CHOOSE_DIR, NULL, applicationPath);
	ZOE_REPORT(izoe, ABORT, LQ5)
	archiveParam.applicationPath = applicationPath;
	lines[6] = zoe_getnextfreeline();
L07:
	izoe = zoe_file_chooser(&id, lines[6], 1, Q7, homedir, CHOOSE_FILE, NULL, applicationLicenseFile);
	ZOE_REPORT(izoe, ABORT, L06);
	archiveParam.applicationLicenseFile = applicationLicenseFile;
	lines[7] = zoe_getnextfreeline();
L08:
	izoe = zoe_close_question(&id, lines[7], 1, Q8,  yesno, 2);
	ZOE_REPORT(izoe, ABORT, L07);
	if (izoe == 0 ) archiveParam.applicationFreeLicence = 1;
	else archiveParam.applicationFreeLicence = 0;
	lines[8] = zoe_getnextfreeline();
L09:
	izoe = zoe_close_question(&id, lines[8], 1, Q9, yesno, 2);
	ZOE_REPORT(izoe, ABORT, L08);
	if (izoe == 0 ) {
		izoe = zoe_file_chooser(&id, zoe_getnextfreeline(), 1, Q10, homedir, CHOOSE_FILE, NULL, applicationPostInstallationScript);
		ZOE_REPORT(izoe, ABORT, L09);
		archiveParam.applicationPostInstallationScript = applicationPostInstallationScript;
	} else
		archiveParam.applicationPostInstallationScript = NULL;
	lines[9] = zoe_getnextfreeline() + 1;
L10:
	izoe = zoe_close_question(&id, lines[9], 1, Q11, yesno, 2);
	ZOE_REPORT(izoe, ABORT, L09);
	if (izoe == 1)
		goto ABORT;
	//---- Summary --------------------------------------------------------------
	zoe_end();

	fprintf(stdout, "\n%s\n\n", TITLE);
	fprintf(stdout, "%s%s\n", Q1, archiveParam.archiveName);
	fprintf(stdout, "%s%s\n", Q2, archiveParam.applicationName);
	fprintf(stdout, "%s%s\n", Q3, archiveParam.applicationRevision);
	fprintf(stdout, "%s%s\n", Q4, archiveParam.applicationDate);
	fprintf(stdout, "%s%s\n", Q5, archiveParam.applicationDescription);
	fprintf(stdout, "%s%s\n", Q6, archiveParam.applicationPath);
	fprintf(stdout, "%s%s\n", Q7, archiveParam.applicationLicenseFile);
	fprintf(stdout, "%s%s\n", Q8, archiveParam.applicationFreeLicence? "Yes":"No");
	fprintf(stdout, "%s%s\n", Q9, (archiveParam.applicationPostInstallationScript == NULL) ? "No":"Yes");
	if (archiveParam.applicationPostInstallationScript != NULL)
		fprintf(stdout, "%s%s\n", Q10, archiveParam.applicationPostInstallationScript);
	fprintf(stdout, "\n");
	//---- Execution -----------------------------------------------------------
	if ((report = tom_genarchiv(archiveParam, NOSILENT, &errorCode)) == EXIT_SUCCESS) {
		fprintf(stdout, "\nSelf extractable archive '%s' successfully generated.\n\n", archiveParam.archiveName);
	} else {
		fprintf(stdout, "\nERROR %d: %s\n\n", errorCode, tom_latesterror());
	}
	//---- Exit ----------------------------------------------------------------
END:
	cmdline_parser_yaTom_free(&args_info);
	return report;
ABORT:
	zoe_end();
	report = EXIT_FAILURE;
	goto END;
}
//------------------------------------------------------------------------------
