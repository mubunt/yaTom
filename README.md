 # *yaTom*, A command-line utility, with a semi-graphic interface, to generate a self-extractable archive..

**yaTom** is another command-line utility running on LINUX that generates a self-extractable compressed tar archive from a directory. The resulting file appears as a shell script, and can be launched as is. The archive will then uncompress itself via a temporary directory and an optional arbitrary command will be executed (for example an installation script). This is pretty similar to archives generated with "makeself". **yaTom** archives also checksums for integrity self-validation (MD5 checksums).

Refer to [**tom**](https://gitlab.com/mubunt/tom) utility and the [**libTom**](https://gitlab.com/mubunt/libtom) library.

## LICENSE
**yaTom** is covered by the GNU General Public License (GPL) version 3 and above.

## USAGE
``` bash
$ yaTom -h
yaTom -h
yaTom - Copyright (c) 2020, Michel RIZZO. All Rights Reserved.
yaTom - Version 1.0.0

A command-line utility, with a semi-graphic interface, to generate a
self-extractable archive.

Usage: yaTom [OPTIONS]...

  -h, --help     Print help and exit
  -V, --version  Print version and exit

Exit: returns a non-zero status if an error is detected.

$ yaTom -V
yaTom - Copyright (c) 2020, Michel RIZZO. All Rights Reserved.
yaTom - Version 1.0.0

$ yaTom 

```
![Example 1](./README_images/yaTom01.png  "Progress 1")

![Example 1](./README_images/yaTom02.png  "Progress 2")

![Example 1](./README_images/yaTom03.png  "Progress 3")

## STRUCTURE OF THE APPLICATION
This section walks you through **yaTom**'s structure. Once you understand this structure, you will easily find your way around in **yaTom**'s code base.

``` bash
$ yaTree
./                  # Application level
├── README_images/  # Images for documentation
│   ├── yaTom01.png # 
│   ├── yaTom02.png # 
│   └── yaTom03.png # 
├── src/            # Source directory
│   ├── Makefile    # Makefile
│   ├── yaTom.c     # Source file
│   └── yaTom.ggo   # 'gengetopt' option definition. Refer to https://www.gnu.org/software/gengetopt/gengetopt.html
├── COPYING.md      # GNU General Public License markdown file
├── LICENSE.md      # License markdown file
├── Makefile        # Makefile
├── README.md       # ReadMe markdown file
├── RELEASENOTES.md # Release Notes markdown file
└── VERSION         # Version identification text file

2 directories, 12 files

$ 
```
## HOW TO BUILD THIS APPLICATION
```Shell
$ cd yaTom
$ make clean all
```

## HOW TO INSTALL AND USE THIS APPLICATION
```Shell
$ cd yaTom
$$ make release
    # Executable generated with -O2 option, is installed in $BIN_DIR directory (defined at environment level).
    # We consider that $BIN_DIR is a part of the PATH.
```

## SOFTWARE REQUIREMENTS
- For usage, nothing particular...
- For development:
   - *GengetOpt* binary package installed, version 2.22.6.
   - *[libTom](https://gitlab.com/mubunt/libtom)*, a C library for LINUX to generate a self-extractable archive.
   - *[libZoe](https://gitlab.com/mubunt/libZoe)*, a C library for LINUX providing an API for developing simple text-based environments.
- Developped and tested on XUBUNTU 19.04, GCC v8.3.0

## RELEASE NOTES
Refer to file [RELEASENOTES](./RELEASENOTES.md).

***